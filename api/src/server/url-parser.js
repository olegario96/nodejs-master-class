const url = require('url');

const REMOVE_LEADING_ENDING_SLASHES_REGEX = /^\/+|\/+$/g;
const SHOULD_PARSE_QUERY_URL = true;

/**
 * Parses URL from request by removing its leading and ending slashes
 * @param {string} reqUrl
 * @return {string}
 */
const parseUrl = (reqUrl) => {
  const parsedUrl = url.parse(reqUrl, SHOULD_PARSE_QUERY_URL);
  const { pathname: path } = parsedUrl;
  return path.replace(REMOVE_LEADING_ENDING_SLASHES_REGEX, '');
}

module.exports = {
  parseUrl,
};
