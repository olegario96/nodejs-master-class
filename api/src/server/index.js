const http = require('http');
const { parseUrl } = require('./url-parser');

const SERVER_PORT = Number(process.env.SERVER_PORT);

const server = http.createServer((req, res) => {
  const urlParsed = parseUrl(req.url);
  res.end('Hello, world!\n');
  console.log(urlParsed);
});

server.listen(SERVER_PORT, () => {
  console.log(`Server is listening on ${SERVER_PORT}`);
});
